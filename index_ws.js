const qrcode = require('qrcode-terminal');

const express = require('express');

const { Client } = require('whatsapp-web.js');
const client = new Client();

var app = express();
app.listen(3000);

var msg;

client.on('qr', qr => {
    console.log(qr);
    qrcode.generate(qr, {small: true});
});

client.on('ready', () => {
    console.log('Client is ready!');
});

client.on('message', async message => {
    msg = message;

    if(message.hasMedia){
        const attachmentData = await message.downloadMedia();
        console.log(attachmentData.mimetype);
    }else{
        console.log(message);
    }

    client.sendMessage(
        message.from,
        message.body
    );
});

client.initialize();



app.get('/',  async (req, res) =>{
    
    let params = req.query;
    console.log(params);
    
    client.sendMessage(
        '593' + params.to + '@c.us',
        params.text
    );
    /*const attachmentData = await msg.downloadMedia();

    //console.log(attachmentData);

    let img = `<img src="data:image/png;base64, ${attachmentData ? attachmentData.data : ''}">`;*/
    res.send('OK');
});