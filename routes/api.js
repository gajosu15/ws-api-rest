const express = require("express");
const messageCtrl = require("../controllers/sendMessage");
const apiTokenCtrl = require("../controllers/apiToken");
const api = express.Router();
const { body, query } = require('express-validator');

api
    .route("/send/text")
    .post(
        body('to').isNumeric(),
        body('msg').isString(),
        query('api_key').isString(),
        messageCtrl.sendText
    );

api
    .route("/send/media")
    .post(messageCtrl.sendMedia);

api
    .route("/send/location")
    .post(messageCtrl.sendLocation);

api
    .route("/send/contact")
    .post(messageCtrl.sendMedia);

module.exports = api;