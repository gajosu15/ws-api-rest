'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ApiTokens extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ApiTokens.init({
    api_key: DataTypes.STRING,
    name: DataTypes.STRING,
    session: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ApiTokens',
  });
  return ApiTokens;
};