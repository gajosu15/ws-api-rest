const dotenv = require('dotenv');
dotenv.config();
const app_port = process.env.HTTP_SERVER_PORT;
const express = require("express");
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const { Client, LocalAuth } = require('whatsapp-web.js');
const qrCode = require('qrcode')
const db = require("./models");
const tokenTool = require("./tools/tokenTools");


const api = require("./routes/api");
const web = require("./routes/web");

io.on("connection",  async (socket) => {

    const apiKey = tokenTool.generateToken(100);

    const wsClient =new Client({
        authStrategy: new LocalAuth({
            clientId: apiKey 
        }),
        puppeteer: {
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--unhandled-rejections=strict'
        ]}
    });

    wsClient.initialize().catch(_ => _);

    wsClient.on('qr', async qr => {
        const qrImage = await qrCode.toDataURL(qr, {width: 500});
        console.log(qr);
        socket.emit('qr_code', {data: qrImage})
    });

    wsClient.on('authenticated', async(session) => {
        let data = {
            api_key: apiKey,
            name: 'ws_default',
            session: JSON.stringify([]) //deprecated
        };
        const token = await db.ApiTokens.create(data);
        socket.emit('api_key', {api_key: data.api_key});
        console.log('AUTHENTICATED', data, token);

        await wsClient.destroy().then( e => {
            console.log('ws client destroy');
        });
    });

    socket.on("disconnect", async (reason) => {
        await wsClient.destroy().then( e => {
            console.log('ws client destroy');
        });
        
    });

});

server.listen(app_port, () => {
    console.log('listening on *:' + app_port);
});

app.use(express.json());
app.use("/api", api);
app.use("/", web);
