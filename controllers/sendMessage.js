
const db = require("../models");
const { Client, LocalAuth } = require('whatsapp-web.js');
const { validationResult } = require('express-validator');

let ctr = {};
let queueClient = {};

ctr.sendText = async (req, res) =>{

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }


    let api_key = await ctr.getApiToken(req.query);
    
    if(api_key){
        
        ctr.client(api_key, async (client)=>{
            console.log('cliente listo ' + api_key)
            client.sendMessage(
                req.body.to + '@c.us',
                req.body.msg
            )
        });

        return res.status(200).json({sucess: 'sended'});
        
    }else{
        return res.status(200).json({error: 'api_key not found'});
    }
    

}

ctr.sendMedia = async (req, res) =>{
}

ctr.sendLocation = async (req, res) =>{
}

ctr.sendContact = async (req, res) =>{
}

ctr.getApiToken = async (params) => {
    let api_key = params.api_key;

    return await db.ApiTokens.findOne({
        where: {
            api_key: api_key
        }
    });
}


ctr.deleteApiToken = async (api_key) => {

    return await db.ApiTokens.destroy({
        where: {
            api_key: api_key
        }
    });
}

ctr.client = async (api_key, callback) => {

    let is_ready = false;

    if(!queueClient[api_key.api_key]){
        const client = new Client({puppeteer: {
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--unhandled-rejections=strict'
            ]}, authStrategy: new LocalAuth({
                clientId: api_key.api_key 
            }),
        });
        client.initialize().catch(e => {
            console.log('error al inicializar cliente', e);
        });
        
        queueClient[api_key.api_key] = client

        client.on('ready', () => {
            is_ready = true;
            callback(client);
        });

        //if the callback is not called, return exception
        setTimeout(async () => {
            if(!is_ready){

                await client.destroy();
                if(queueClient[api_key.api_key]){
                    clearInterval(queueClient[api_key.api_key].killCountDown);
                    delete queueClient[api_key.api_key];
                    api_key.destroy()
                }

                ctr.deleteApiToken(api_key.api_key);
                // Fired if session restore was unsuccessfull
                console.error('AUTHENTICATION FAILURE');
                //throw new Error('client not ready');
            }
        }, 30000);


        client.on('auth_failure', async msg => {

            
        });

        console.log('instancia creada ' + api_key.id)

    }else{
        const client = queueClient[api_key.api_key];
        callback(client);
        console.log('instancia reciclada ' + api_key.id)
    }


    if(queueClient[api_key.api_key].killCountDown){
        clearTimeout(queueClient[api_key.api_key].killCountDown);
        console.log("Matador matado "  + api_key.id)
    }

    queueClient[api_key.api_key].killCountDown = setTimeout( async() => {
        await queueClient[api_key.api_key].destroy();
        delete queueClient[api_key.api_key];
        console.log('instancia eliminada' + api_key.id)
    }, 60000);

    
}

module.exports = ctr;